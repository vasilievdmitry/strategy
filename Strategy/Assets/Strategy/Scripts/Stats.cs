using System;

[Serializable]
public class Stats
{
    public float movementSpeed;
    public float rotationSpeed;
    public float health;
}
