using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;

public abstract class IAlive : MonoBehaviour
{
    [EnumToggleButtons] public InspectorDebug debug;

    [BoxGroup("Setup data"), HideLabel] public Stats stats;

    [ShowIf("debug", InspectorDebug.on)] [ReadOnly] public NavMeshAgent agent;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.speed = stats.movementSpeed;
        agent.angularSpeed = stats.rotationSpeed;
    }
}