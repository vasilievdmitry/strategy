using System;
using Sirenix.OdinInspector;
using UnityEngine;

[Serializable]
public class ResourceData
{
    public Action valueChanged;
    private float count;
    
    [ShowInInspector]
    public float currentCount
    {
        get => count;
        set
        {
            count = value;
            valueChanged?.Invoke();
        }
    }

    public override string ToString()
    {
        return currentCount.ToString();
    }

    public ResourceData FromString(string value)
    {
        ResourceData result = new ResourceData();
        result.currentCount = float.Parse(value);
        return result;
    }
}

[Serializable]
public class ResourcesDictionary : UnitySerializedDictionary<ResourceName, ResourceData>
{
}

public enum ResourceName
{
    gold,
    wood,
    stone
}
