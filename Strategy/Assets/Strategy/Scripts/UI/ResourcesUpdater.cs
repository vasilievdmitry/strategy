using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

public class ResourcesUpdater : MonoBehaviour
{
    [BoxGroup("Setup data")] public GameObject resourceUIPrefab;
    [BoxGroup("Setup data")] public Transform prefabHolder;
    
    [EnumToggleButtons]
    public InspectorDebug debug;

    [Title("AutoFind data")] 
    [ShowIf("debug", InspectorDebug.on)][ReadOnly] public Player player;

    [Title("Generic data")] 
    [ShowIf("debug", InspectorDebug.on)][ReadOnly] public Dictionary<ResourceName, ResourceUI> resources;

    private void OnValidate()
    {
        var allResources = Enum.GetValues(typeof(ResourceName)).Cast<ResourceName>().ToList();
        foreach (var resourceName in allResources)
        {
            var searchName = $"<sprite=\"{resourceName}\" index=0>";
            
        }
    }

    private void Start()
    {
        player = FindObjectOfType<Player>();
        resources = new Dictionary<ResourceName, ResourceUI>();

        foreach (var resKVP in player.resourcesBag)
        {
            var resource = Instantiate(resourceUIPrefab, prefabHolder).GetComponent<ResourceUI>();
            resources.Add(resKVP.Key, resource);
            resource.name = resKVP.Key;
            resKVP.Value.valueChanged += UpdateResourcesUI;
        }
        UpdateResourcesUI();
    }

    [ShowIf("debug", InspectorDebug.on)][Button]
    private void UpdateResourcesUI()
    {
        foreach (var resource in resources)
        {
            foreach (var resKVP in player.resourcesBag)
            {
                if(resKVP.Key != resource.Key) continue;
                resource.Value.countOut.text = $"<sprite=\"{resKVP.Key}\" index=0> " + resKVP.Value.currentCount;
                resource.Value.transform.gameObject.SetActive(resKVP.Value.currentCount > 0);
            }
        }        
    }
}
