using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CivilianTypeChanger : MonoBehaviour
{
    [Header("-Editor DragNDrop data-")]
    [Required] public Transform buttonsParent;
    [Required] public Civilian civilian;

    void Start()
    {
        //todo fix type setter
        // var allCivilianTypes = Enum.GetValues(typeof(CivilianType)).Cast<CivilianType>().ToList();
        // for (int count = 0; count < buttonsParent.childCount; count++)
        // {
        //     if (count > allCivilianTypes.Count)
        //     {
        //         buttonsParent.GetChild(count).gameObject.SetActive(false);
        //         continue;
        //     }
        //     buttonsParent.GetChild(count).GetComponent<Button>().onClick.AddListener(()=> civilian.type = allCivilianTypes[count]);
        //     buttonsParent.GetChild(count).GetChild(0).GetComponent<TextMeshProUGUI>().text = allCivilianTypes[count].ToString();
        // }        
    }
}
