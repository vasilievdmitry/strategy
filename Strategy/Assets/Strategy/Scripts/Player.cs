using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;

public class Player : IAlive
{
    #region Fileds
    
    [ShowIf("debug", InspectorDebug.on)] [ReadOnly] public FloatingJoystick joystick;
    [ShowIf("debug", InspectorDebug.on)] [ReadOnly] public Vector3 movementDirection;
    [ShowIf("debug", InspectorDebug.on)] public ResourcesDictionary resourcesBag;
    private InspectorDebug debug1;

    #endregion
    
    private void Awake()
    {
        joystick = FindObjectOfType<FloatingJoystick>();
        
        var allResources = Enum.GetValues(typeof(ResourceName)).Cast<ResourceName>().ToList();
        foreach (var resourceName in allResources)
        {
            resourcesBag.Add((ResourceName) Enum.Parse(typeof(ResourceName), resourceName.ToString()), 
                new ResourceData().FromString(PlayerPrefs.GetString(resourceName.ToString(), "0")));
        }
    }

    private void Update()
    {
        movementDirection = Vector3.forward * joystick.Vertical + Vector3.right * joystick.Horizontal;
        
        if (movementDirection != Vector3.zero)
            transform.forward = Vector3.Lerp(transform.forward, movementDirection, Time.deltaTime * stats.rotationSpeed);
        
        agent.velocity = transform.forward * stats.movementSpeed * Mathf.Abs(movementDirection.magnitude);
    }

    private void OnApplicationQuit()
    {
        foreach (var resKVP in resourcesBag)
        {
            PlayerPrefs.SetString(resKVP.Key.ToString(), resKVP.Value.ToString());
        }
    }
}

public enum InspectorDebug
{
    off,
    on
}
