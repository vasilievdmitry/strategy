using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;

public class VoxelTile : MonoBehaviour
{
    public float voxelSize = 0.1f;
    public int voxelCountPerSide = 10;

    [EnumToggleButtons] public InspectorDebug debug;

    [ShowIf("debug", InspectorDebug.on)] [ReadOnly] public byte[] colorsForward; 
    [ShowIf("debug", InspectorDebug.on)] [ReadOnly] public byte[] colorsBack; 
    [ShowIf("debug", InspectorDebug.on)] [ReadOnly] public byte[] colorsRight; 
    [ShowIf("debug", InspectorDebug.on)] [ReadOnly] public byte[] colorsLeft;

    [Button]
    public void SetColors()
    {
        colorsForward = new byte[voxelCountPerSide * voxelCountPerSide];
        colorsBack = new byte[voxelCountPerSide * voxelCountPerSide];
        colorsRight = new byte[voxelCountPerSide * voxelCountPerSide];
        colorsLeft = new byte[voxelCountPerSide * voxelCountPerSide];
        
        for (int verticalLayer = 0; verticalLayer < voxelCountPerSide; verticalLayer++)
        {
            for (int horizontalLayer = 0; horizontalLayer < voxelCountPerSide; horizontalLayer++)
            {
                colorsForward[verticalLayer * voxelCountPerSide + horizontalLayer] = GetVoxelColor(verticalLayer, horizontalLayer, Vector3.forward);
                colorsBack[verticalLayer * voxelCountPerSide + horizontalLayer] = GetVoxelColor(verticalLayer, horizontalLayer, Vector3.back);
                colorsRight[verticalLayer * voxelCountPerSide + horizontalLayer] = GetVoxelColor(verticalLayer, horizontalLayer, Vector3.right);
                colorsLeft[verticalLayer * voxelCountPerSide + horizontalLayer] = GetVoxelColor(verticalLayer, horizontalLayer, Vector3.left);
            }
        }
    }

    private byte GetVoxelColor(int verticalLayer, int horizontalOffset, Vector3 direction)
    {
        var meshCollider = GetComponentInChildren<MeshCollider>();
        Vector3 rayStart = Vector3.zero;

        var halfVoxel = voxelSize / 2;
        
        if (direction == Vector3.forward)
        {
            rayStart = meshCollider.bounds.min + new Vector3(halfVoxel + horizontalOffset * voxelSize, 0, -halfVoxel);
        }
        if (direction == Vector3.back)
        {
            rayStart = meshCollider.bounds.max + new Vector3(-halfVoxel - (voxelCountPerSide - horizontalOffset - 1) * voxelSize, 0, halfVoxel);
        }
        if (direction == Vector3.right)
        {
            rayStart = meshCollider.bounds.min + new Vector3(-halfVoxel, 0, halfVoxel + horizontalOffset * voxelSize);
        }
        if (direction == Vector3.left)
        {
            rayStart = meshCollider.bounds.max + new Vector3(halfVoxel, 0, -halfVoxel - (voxelCountPerSide - horizontalOffset - 1) * voxelSize);
        }

        rayStart.y = meshCollider.bounds.min.y + halfVoxel + verticalLayer * voxelSize;

        // Debug.DrawRay(rayStart, direction * 0.1f, Color.blue, 2);
        
        if (Physics.Raycast(new Ray(rayStart, direction), out RaycastHit hit, voxelSize))
        {
            var hitVertex = meshCollider.sharedMesh.triangles[hit.triangleIndex*3];
            var uvColorCords = meshCollider.sharedMesh.uv[hitVertex];
            var colorIndex = (byte) (uvColorCords.x * 256);
            return colorIndex;
        }

        return 0;
    }
}
