using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.EditorTools;
using UnityEngine;
using Random = UnityEngine.Random;

public class LevelGenerator : MonoBehaviour
{
    public VoxelTile[] tilesPrefabs;
    public VoxelTile[,] spawnedTiles;
    public Vector2Int levelSize = new Vector2Int(10,10);
    void Start()
    {
        spawnedTiles = new VoxelTile[levelSize.x, levelSize.y];

        foreach (var tile in tilesPrefabs)
        {
            tile.SetColors();
        }
        
        StartCoroutine(Generate());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StopAllCoroutines();
            foreach (var tile in spawnedTiles) if(tile != null) Destroy(tile.gameObject);
            StartCoroutine(Generate());
        }
    }

    public IEnumerator Generate()
    {
        PlaceTile(levelSize.x/2, levelSize.y/2);

        while (true)
        {
            Vector2Int pos;
            while (true)
            {
                pos = new Vector2Int(Random.Range(1, levelSize.x-1),Random.Range(1, levelSize.y-1));
                
                bool hasTileClose = spawnedTiles[pos.x + 1, pos.y] != null || spawnedTiles[pos.x - 1, pos.y] != null ||
                                    spawnedTiles[pos.x, pos.y + 1] != null || spawnedTiles[pos.x, pos.y - 1] != null;

                if (spawnedTiles[pos.x, pos.y] == null && hasTileClose)
                {
                    Debug.Log("Found place for tile!!!");
                    break;
                }
            }
            
            PlaceTile(pos.x, pos.y);
            yield return new WaitForSeconds(0.2f);
        }
    }

    private void PlaceTile(int x, int y)
    {
        List<VoxelTile> availableTiles = new List<VoxelTile>();

        foreach (var tile in tilesPrefabs)
        {
            if (CanAppendTile(spawnedTiles[x-1, y], tile, Vector3.left) &&
                CanAppendTile(spawnedTiles[x+1, y], tile, Vector3.right) &&
                CanAppendTile(spawnedTiles[x, y-1], tile, Vector3.back) &&
                CanAppendTile(spawnedTiles[x, y+1], tile, Vector3.forward))
            {
                availableTiles.Add(tile);
                Debug.Log("Found appendable tile: " + tile.gameObject.name);
            }
        }

        if (availableTiles.Count == 0)
        {
            Debug.Log("NO AVAILABLE TILES :C");
            return;
        }

        var randomTile = availableTiles[Random.Range(0, availableTiles.Count)];
        spawnedTiles[x,y] = Instantiate(randomTile, new Vector3(x, 0, y), Quaternion.identity, transform);
        Debug.Log("---CREATE TITLE " + spawnedTiles[x,y].gameObject.name + "---");
        spawnedTiles[x,y].GetComponent<VoxelTile>().SetColors();
    }

    private bool CanAppendTile(VoxelTile existingTile, VoxelTile tileToAppend, Vector3 direction)
    {
        bool res = false;
        if (existingTile == null)
        {
            Debug.Log("NO EXISTING TILE!!!!!!!!");
            return true;
        }
        
        // var spawnedVersion = Instantiate(tileToAppend, Vector3.zero, Quaternion.identity);
        // spawnedVersion.SetColors();
        // tileToAppend = spawnedVersion.GetComponent<VoxelTile>();
        // existingTile.SetColors();
        
        if (direction == Vector3.right) res = Enumerable.SequenceEqual(existingTile.colorsRight, tileToAppend.colorsLeft);
        if (direction == Vector3.left) res = Enumerable.SequenceEqual(existingTile.colorsLeft, tileToAppend.colorsRight);
        if (direction == Vector3.forward) res = Enumerable.SequenceEqual(existingTile.colorsForward, tileToAppend.colorsBack);
        if (direction == Vector3.back) res = Enumerable.SequenceEqual(existingTile.colorsBack, tileToAppend.colorsForward);
        
        if(res) Debug.Log(existingTile.gameObject.name + " connection to " + tileToAppend.gameObject.name + " by " + direction + " is " + (res ? "aprooved": "incorrect"));

        if (res)
        {
            if (direction == Vector3.right)
            {
                Debug.Log(string.Join(",", existingTile.colorsRight));
                Debug.Log(string.Join(",", tileToAppend.colorsLeft));
            }
            if (direction == Vector3.left)
            {
                Debug.Log(string.Join(",", existingTile.colorsLeft));
                Debug.Log(string.Join(",", tileToAppend.colorsRight));
            }
            if (direction == Vector3.back)
            {
                Debug.Log(string.Join(",", existingTile.colorsBack));
                Debug.Log(string.Join(",", tileToAppend.colorsForward));
            }
            if (direction == Vector3.forward)
            {
                Debug.Log(string.Join(",", existingTile.colorsForward));
                Debug.Log(string.Join(",", tileToAppend.colorsBack));
            }
        }
        
        // Destroy(spawnedVersion.gameObject);
        return res;
    }
}
