using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public bool isOpen;
    public bool needOpen;
    public bool needOpenOnce;
    private void OnTriggerStay(Collider other)
    {
        if(!other.transform.CompareTag("Player")) return;
        if(Input.GetKeyDown(KeyCode.Space)) Open();
    }

    private void Open()
    {
        transform.rotation = Quaternion.Lerp(Quaternion.Euler(0, isOpen ? 0 : 90, 0), transform.rotation, 0.000000001f);
        isOpen = !isOpen;
    }

    private void Update()
    {
        if (needOpenOnce)
        {
            Open();
            needOpenOnce = !needOpenOnce;
        }

        if(!needOpen) return;
        
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, isOpen ? 0 : 90, 0), Time.deltaTime);
    }
}
