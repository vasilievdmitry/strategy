using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;
using Random = UnityEngine.Random;

public class Civilian : IAlive
{
    #region Fields

    [Header("-Changable data-")] 
    public float playerDetectRadius;
    
    [Header("-Editor DragNDrop data-")]
    [Required] public Transform typeChangerUI;
    
    [ShowIf("debug", InspectorDebug.on)] public CivilianType type;
    [ShowIf("debug", InspectorDebug.on)] [ReadOnly] public LookAtConstraint lookAtConstraint;
    [ShowIf("debug", InspectorDebug.on)] [ReadOnly] public Player player;

    [ShowIf("type", CivilianType.follower)] public float followRange;
    [ShowIf("type", CivilianType.follower)] public float playerRadius;

    #endregion
    private void OnValidate()
    {
        var collider = transform.GetComponent<SphereCollider>();
        if (collider == null) collider = gameObject.AddComponent<SphereCollider>();
        if (!collider.isTrigger) collider.isTrigger = true;
        if (collider.radius != playerDetectRadius) collider.radius = playerDetectRadius;
    }

    public void Update()
    {
        if (type == CivilianType.follower)
        {
            if (player == null) player = FindObjectOfType<Player>();
            
            var currentDistance = Vector3.Distance(transform.position, player.transform.position);
            
            if(currentDistance < followRange) return;
            
            if(agent.velocity != Vector3.zero) return;
            
            var movePosition = new Vector3(player.transform.position.x + Random.Range(-playerRadius, playerRadius), 
                                              player.transform.position.y, 
                                           player.transform.position.z + Random.Range(-playerRadius, playerRadius));
            
            agent.SetDestination(movePosition);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player") return;
        if (lookAtConstraint == null) lookAtConstraint = typeChangerUI.gameObject.AddComponent<LookAtConstraint>();
        if (lookAtConstraint.sourceCount == 0) lookAtConstraint.AddSource(new ConstraintSource {sourceTransform = Camera.main.transform, weight = 1});
        if (typeChangerUI.transform.localScale == Vector3.zero) typeChangerUI.transform.localScale = Vector3.one;
        lookAtConstraint.constraintActive = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag != "Player") return;
        if (typeChangerUI.transform.localScale == Vector3.one) typeChangerUI.transform.localScale = Vector3.zero;
    }
}

public enum CivilianType
{
    none,
    follower,
    worker
}
